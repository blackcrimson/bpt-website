<?php
use sgpbm\MailchimpApi;
$apiKey = get_option('SGPB_MAILCHIMP_API_KEY');
$status = MailchimpApi::isConnected();
?>
<div class="sgpb-wrapper sgpb-settings">
	<div class="row">
		<div class="col-md-6">
			<h2><?php _e('MailChimp Settings', SG_POPUP_TEXT_DOMAIN); ?></h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="postbox-container-2" class="postbox-container">
					<div id="normal-sortables" class="meta-box-sortables ui-sortable">
						<div class="postbox popup-builder-special-postbox">
							<div class="handlediv js-special-title" title="Click to toggle"><br></div>
							<h3 class="hndle ui-sortable-handle js-special-title">
								<span><?php _e('API Settings', SG_POPUP_TEXT_DOMAIN); ?></span>
							</h3>
							<div class="sgpb-options-content">
							<form action="<?php echo SG_POPUP_ADMIN_URL;?>admin-post.php?action=sgpb_save_mailchimp_api_key" method="POST">
								<?php
								if(function_exists('wp_nonce_field')) {
									wp_nonce_field('sgpbPopupBuilderMailchimpApiKeySave');
								}
								?>
								<div class="row form-group">
									<div class="col-md-3">
										<label><?php _e('Status', SG_POPUP_TEXT_DOMAIN)?></label>
									</div>
									<div class="col-md-6">
										<?php if(!$status): ?>
											<span class="sg-mailchimp-connect-status sg-mailchimp-not-connected"><?php _e('Not connected', SG_POPUP_TEXT_DOMAIN)?></span>
										<?php else : ?>
											<span class="sg-mailchimp-connect-status sg-mailchimp-connected"><?php _e('Connected', SG_POPUP_TEXT_DOMAIN)?></span>
										<?php endif;?>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<label id="sgpb-api-key-label" for="sgpb-mailchimp-api-key"><?php _e('API Key', SG_POPUP_TEXT_DOMAIN)?></label>
									</div>
									<div class="col-md-9">
										<div class="sg-apikey-input-div">
											<input type="password" class="widefat" placeholder="Your MailChimp API key" id="sgpb-mailchimp-api-key" name="mailchimp-api-key" value="<?php echo esc_attr($apiKey); ?>" autocomplete="off">
										</div>
										<div class="sg-show-apikey-div">
											<input type="checkbox" id="sg-show-mailchimp-apikey"><span class="sgpb-mailchimp-api-key-show"><?php _e('Show', SG_POPUP_TEXT_DOMAIN); ?></span>
										</div>
										<div class="clear"></div>
										<p class="sgpb-mailchimp-help"><span><?php _e('The API key for connecting with your MailChimp account', SG_POPUP_TEXT_DOMAIN); ?>.</span>  <a class="sgpb-mailchimp-to-api-link" target="_blank" href="https://admin.mailchimp.com/account/api"><?php _e('Get your API key here', SG_POPUP_TEXT_DOMAIN)?>.</a></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" class="button button-primary" value="<?php _e('Save Changes', SG_POPUP_TEXT_DOMAIN)?>">
									</div>
								</div>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
