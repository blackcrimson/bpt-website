<?php
	use sgpbm\MailchimpApi;
	use sgpb\AdminHelper;
	use sgpbm\DefaultOptionsData;
	use sgpb\MultipleChoiceButton;
	$defaultData = DefaultOptionsData::getDefaultData();
	$status = MailchimpApi::isConnected();
	if ($status) {
		$sgpbMailchimp = MailchimpApi::getInstance();
		$count = $sgpbMailchimp->getTotalCount();
	}
	$selectedFormId = $popupTypeObj->getOptionValue('sgpb-mailchimp-lists');
	$mailchimpFormSubPopups = $popupTypeObj->getPopupsIdAndTitle();
	$id = (int)@$_GET['post'];
?>
<div class="sgpb-wrapper sgpb-mailchimp-options-wrapper">
<div class="sgpb-wrapper">
<div class="row">
<div class="col-md-6">
	<div class="row form-group">
		<div class="col-md-5">
			<label><?php _e('Status', SG_POPUP_TEXT_DOMAIN)?></label>
		</div>
		<div class="col-md-6">
			<?php if (!$status): ?>
				<span class="sg-mailchimp-connect-status sg-mailchimp-not-connected"><?php _e('Not connected', SG_POPUP_TEXT_DOMAIN)?></span>
			<?php else : ?>
				<span class="sg-mailchimp-connect-status sg-mailchimp-connected"><?php _e('Connected', SG_POPUP_TEXT_DOMAIN)?></span>
			<?php endif;?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label class="sgpb-selectbox-label"><?php _e('Your lists', SG_POPUP_TEXT_DOMAIN)?></label>
		</div>
		<div class="col-md-6">
			<?php
			if (!$status): ?>
				<a class="sgpb-connect-link" href="<?php echo admin_url().'./edit.php?post_type='.SG_POPUP_POST_TYPE.'&page='.SGPB_POPUP_TYPE_MAILCHIMP ?>"><?php _e('Setup your MailChimp Api key')?></a>
			<?php
			elseif (!$count):
				_e('You don\'t have any lists yet. Please create a list first', SG_POPUP_TEXT_DOMAIN);
			else:
				echo AdminHelper::createSelectBox($sgpbMailchimp->getListsIdAndTitle(), $selectedFormId, array('name' => 'sgpb-mailchimp-lists', 'class'=>'sgpb-mailchimp-lists js-sg-select2'));
			endif;
			?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label class="sgpb-mailchimp-label-bold"><?php _e('General settings', SG_POPUP_TEXT_DOMAIN)?></label>
		</div>
		<div class="col-md-6"></div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-enable-double-optin"><?php _e('Enable double opt-in', SG_POPUP_TEXT_DOMAIN)?></label>
		</div>
		<div class="col-md-6">
			<input type="checkbox" name="sgpb-enable-double-optin" id="sgpb-enable-double-optin" <?php echo $popupTypeObj->getOptionValue('sgpb-enable-double-optin'); ?>>
			<span class="dashicons dashicons-editor-help sgpb-info-icon sgpb-info-icon-align"></span>
			<span class="infoSelectRepeat samefontStyle sgpb-info-text">
				<?php _e('With single opt-in, new subscribers fill out a signup form and are immediately added to a mailing list, even if their address is invalid or contains a typo. Single opt-in can clog your list with bad addresses, and possibly generate spam complaints from subscribers who don’t remember signing up', SG_POPUP_TEXT_DOMAIN)?>.
			</span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-show-required-fields"><?php _e('Show only required fields', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="checkbox" name="sgpb-show-required-fields" id="sgpb-show-required-fields" <?php echo $popupTypeObj->getOptionValue('sgpb-show-required-fields'); ?>>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-enable-asterisk-label"><?php _e('Enable asterisk label', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="checkbox" name="sgpb-enable-asterisk-label" class="js-checkbox-accordion" id="sgpb-enable-asterisk-label" <?php echo $popupTypeObj->getOptionValue('sgpb-enable-asterisk-label'); ?>>
		</div>
	</div>
	<div class="sg-full-width">
		<div class="row form-group">
			<div class="col-md-5">
				<label for="sgpb-enable-asterisk-title" class="sgpb-sub-option"><?php _e('Asterisk title', SG_POPUP_TEXT_DOMAIN)?>:</label>
			</div>
			<div class="col-md-6">
				<input type="checkbox" name="sgpb-enable-asterisk-title" class="js-checkbox-accordion" id="sgpb-enable-asterisk-title" <?php echo $popupTypeObj->getOptionValue('sgpb-enable-asterisk-title'); ?>>
			</div>
		</div>
		<div class="sg-full-width">
			<div class="row form-group">
				<div class="col-md-5">
					<label for="sgpb-mailchimp-asterisk-label" class="sgpb-static-padding-top sgpb-double-sub-option"><?php _e('Asterisk label', SG_POPUP_TEXT_DOMAIN)?>:</label>
				</div>
				<div class="col-md-6">
					<input type="text" name="sgpb-mailchimp-asterisk-label" id="sgpb-mailchimp-asterisk-label" class="form-control sgpb-full-width-events" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-asterisk-label')); ?>">
				</div>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-form-alignment" class="sgpb-selectbox-label"><?php _e('Form alignment', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<?php echo AdminHelper::createSelectBox($defaultData['formAlign'], esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-form-align')), array('name' => 'sgpb-mailchimp-form-align', 'class' => 'sgpb-mailchimp-form-align js-sg-select2'));?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-label-alignment" class="sgpb-selectbox-label"><?php _e('Label alignment', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<?php echo AdminHelper::createSelectBox($defaultData['labelAlign'], esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-label-alignment')), array('name' => 'sgpb-mailchimp-label-alignment', 'class' => 'sgpb-mailchimp-label-alignment js-sg-select2'));?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-required-message" class="sgpb-static-padding-top"><?php _e('Required message', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-required-message" class="form-control sgpb-full-width-events" id="sgpb-mailchimp-required-message" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-required-message')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-email-message" class="sgpb-static-padding-top"><?php _e('Email message', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-email-message" class="form-control sgpb-full-width-events" id="sgpb-mailchimp-email-message" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-email-message')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-email-label" class="sgpb-static-padding-top"><?php _e('Email label', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-email-label" class="form-control sgpb-full-width-events" id="sgpb-mailchimp-email-label" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-email-label')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-error-message" class="sgpb-static-padding-top"><?php _e('Error message', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-error-message" class="form-control sgpb-full-width-events" id="sgpb-mailchimp-error-message" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-error-message')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-error-message"><?php _e('Show form before content', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="checkbox" name="sgpb-mailchimp-show-form-to-top" class="" id="sgpb-mailchimp-show-form-to-top" <?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-show-form-to-top')); ?>>
			<span class="dashicons dashicons-editor-help sgpb-info-icon sgpb-info-icon-align"></span>
			<span class="infoSelectRepeat samefontStyle sgpb-info-text">
				<?php _e('If this option is checked, the form will be displayed before the text', SG_POPUP_TEXT_DOMAIN)?>.
			</span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label class="sgpb-mailchimp-label-bold"><?php _e('General style', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-label-color"><?php _e('Label color', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-label-color">
				<input class="sgpb-mailchimp-label-color" type="text" name="sgpb-mailchimp-label-color" value="<?php echo esc_html($popupTypeObj->getOptionValue('sgpb-mailchimp-label-color')); ?>" >
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label class="sgpb-mailchimp-label-bold"><?php _e('Inputs style', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-input-width" class="sgpb-static-padding-top"><?php _e('Width', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-input-width" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="input" data-style-type="width" id="sgpb-mailchimp-input-width" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-input-width')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-input-height" class="sgpb-static-padding-top"><?php _e('Height', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-input-height" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="input" data-style-type="height" id="sgpb-mailchimp-input-height" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-input-height')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-border-radius" class="sgpb-static-padding-top"><?php _e('Border radius', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-border-radius" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="input" data-style-type="border-radius" id="sgpb-mailchimp-border-radius" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-border-radius')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-border-width" class="sgpb-static-padding-top"><?php _e('Border width', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-border-width" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="input" data-style-type="border-width" id="sgpb-mailchimp-border-width" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-border-width')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-border-color"><?php _e('Border color', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-overlay-color">
				<input class="js-sgpb-colors" type="text" name="sgpb-mailchimp-border-color" data-field-type="input" data-style-type="border-color" value="<?php echo esc_html($popupTypeObj->getOptionValue('sgpb-mailchimp-border-color')); ?>" >
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-background-color"><?php _e('Background color', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-overlay-color">
				<input class="js-sgpb-colors" type="text" name="sgpb-mailchimp-background-color" data-field-type="input" data-style-type="background-color" value="<?php echo esc_html($popupTypeObj->getOptionValue('sgpb-mailchimp-background-color')); ?>" >
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-input-color"><?php _e('Text color', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-overlay-color">
				<input class="js-sgpb-colors" type="text" name="sgpb-mailchimp-input-color" data-field-type="input" data-style-type="color" value="<?php echo esc_html($popupTypeObj->getOptionValue('sgpb-mailchimp-input-color')); ?>" >
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label class="sgpb-mailchimp-label-bold"><?php _e('Submit button style', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-overlay-color">
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-title" class="sgpb-static-padding-top"><?php _e('Button title', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-submit-title" class="form-control sgpb-full-width-events js-mailchimp-submit-title" id="sgpb-mailchimp-submit-title" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-title')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-width" class="sgpb-static-padding-top"><?php _e('Width', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-submit-width" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="submit" data-style-type="width" id="sgpb-mailchimp-submit-width" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-width')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-height" class="sgpb-static-padding-top"><?php _e('Height', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-submit-height" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="submit" data-style-type="height" id="sgpb-mailchimp-submit-height" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-height')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-border-width" class="sgpb-static-padding-top"><?php _e('Border width', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-submit-border-width" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="submit" data-style-type="border-width" id="sgpb-mailchimp-submit-height" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-border-width')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-border-radius" class="sgpb-static-padding-top"><?php _e('Border radius', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="text" name="sgpb-mailchimp-submit-border-radius" class="form-control sgpb-full-width-events js-mailchimp-dimension" data-field-type="submit" data-style-type="border-radius" id="sgpb-mailchimp-submit-border-radius" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-border-radius')); ?>">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-background-color"><?php _e('Background color', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-overlay-color">
				<input class="js-sgpb-colors" type="text" name="sgpb-mailchimp-submit-background-color" data-field-type="submit" data-style-type="background-color" value="<?php echo esc_html($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-background-color')); ?>" >
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-border-color"><?php _e('Border color', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-overlay-color">
				<input class="js-sgpb-colors" type="text" name="sgpb-mailchimp-submit-border-color" data-field-type="submit" data-style-type="border-color" value="<?php echo esc_html($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-border-color')); ?>" >
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-submit-color"><?php _e('Label color', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<div class="sgpb-color-picker-wrapper sgpb-overlay-color">
				<input class="js-sgpb-colors" type="text" name="sgpb-mailchimp-submit-color" data-field-type="submit" data-style-type="color" value="<?php echo esc_html($popupTypeObj->getOptionValue('sgpb-mailchimp-submit-color')); ?>" >
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-6">
			<label class="sgpb-mailchimp-label-bold"><?php _e('After successful subscription', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
		</div>
	</div>
	<?php
		$multipleChoiceButton = new MultipleChoiceButton($defaultData['mailchimpFormSuccessBehavior'], $popupTypeObj->getOptionValue('sgpb-mailchimp-success-behavior'));
		echo $multipleChoiceButton;
	?>
	<div class="sg-hide sg-full-width" id="mailchimp-show-success-message">
		<div class="row form-group">
			<label for="sgpb-mailchimp-success-message" class="col-md-5 control-label">
				<?php _e('Success message', SG_POPUP_TEXT_DOMAIN)?>:
			</label>
			<div class="col-md-7"><input name="sgpb-mailchimp-success-message" id="sgpb-mailchimp-success-message" class="form-control sgpb-full-width-events" value="<?php echo esc_attr($popupTypeObj->getOptionValue('sgpb-mailchimp-success-message')); ?>"></div>
		</div>
	</div>
	<div class="sg-hide sg-full-width" id="mailchimp-redirect-to-URL">
		<div class="row form-group">
			<label for="sgpb-mailchimp-success-redirect-URL" class="col-md-5 control-label">
				<?php _e('Redirect URL', SG_POPUP_TEXT_DOMAIN)?>:
			</label>
			<div class="col-md-7"><input type="url" name="sgpb-mailchimp-success-redirect-URL" id="sgpb-mailchimp-success-redirect-URL" placeholder="https://www.example.com" class="form-control sgpb-full-width-events" value="<?php echo $popupTypeObj->getOptionValue('sgpb-mailchimp-success-redirect-URL'); ?>"></div>
		</div>
		<div class="row form-group">
			<label for="contact-success-redirect-new-tab" class="col-md-5 control-label">
				<?php _e('Redirect to new tab', SG_POPUP_TEXT_DOMAIN)?>:
			</label>
			<div class="col-md-7"><input type="checkbox" name="sgpb-mailchimp-success-redirect-new-tab" id="sgpb-mailchimp-success-redirect-new-tab" placeholder="https://www.example.com" <?php echo $popupTypeObj->getOptionValue('sgpb-mailchimp-success-redirect-new-tab'); ?>></div>
		</div>
	</div>
	<div class="sg-hide sg-full-width" id="mailchimp-open-popup">
		<div class="row form-group">
			<label for="sgpb-mailchimp-success-popup" class="col-md-5 control-label">
				<?php _e('Select popup', SG_POPUP_TEXT_DOMAIN)?>:
			</label>
			<div class="col-md-7">
				<?php echo AdminHelper::createSelectBox($mailchimpFormSubPopups, $popupTypeObj->getOptionValue('sgpb-mailchimp-success-popup'), array('name' => 'sgpb-mailchimp-success-popup', 'class'=>'js-sg-select2 sgpb-full-width-events')); ?>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-5">
			<label for="sgpb-mailchimp-close-popup-already-subscribed"><?php _e('Close popup if user already subscribed', SG_POPUP_TEXT_DOMAIN)?>:</label>
		</div>
		<div class="col-md-6">
			<input type="checkbox" name="sgpb-mailchimp-close-popup-already-subscribed" id="sgpb-mailchimp-close-popup-already-subscribed" <?php echo $popupTypeObj->getOptionValue('sgpb-mailchimp-close-popup-already-subscribed'); ?>>
		</div>
	</div>
</div>
	<div class="col-md-6">
		<?php echo $popupTypeObj->renderCss(); ?>
		<?php if ($status): ?>
		<h1 class="sgpb-mailchomp-live-preview"><?php _e('Live Preview', SG_POPUP_TEXT_DOMAIN); ?></h1>
			<div class="sgpb-live-preview-wrapper">
				<div class="sgpb-mailchimp-admin-form-wrapper sgpbmMailchimpForm sgpb-mailchimp-<?php echo $id; ?>"></div>
				<div class="sgpb-loader sg-hide-element">
					<div class="sgpb-container">
						<div class="sgpb-banner">
							<?php _e('LOADING', SG_POPUP_TEXT_DOMAIN); ?>
							<div class="sgpb-banner-left"></div>
							<div class="sgpb-banner-right"></div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
</div>
</div>
