<?php

/**
 * This class adds a shortcode to display the affilaite links.
 */

class WPTWA_Shortcode {
	
	private static $styles = '';
	private static $button_id = 0;
	
	public function __construct () {
		
		add_shortcode( 'whatsapp', array( $this, 'shortcodeWhatsApp' ) );
		add_shortcode( 'whatsapp_button', array( $this, 'whatsappButton' ) );
		
		add_action( 'wp_footer', array( 'WPTWA_Shortcode', 'addStyles' ) );
		
	}
	
	public function whatsappButton ( $atts, $content = null ) {
		
		extract( shortcode_atts( array(
			'id' => '',
			'button_style' => ''
		), $atts ) );
		
		wp_reset_postdata();
		
		$page_title = get_the_title();
		$page_url = get_permalink();
		
		$account = get_posts( array(
			'posts_per_page' => -1,
			'post__in' => array( $id ),
			'post_type' => 'wptwa_accounts'
		) );
		
		if ( count( $account ) < 1 ) {
			return '';
		}
		
		$item = '';
		foreach ( $account as $post ) {
			setup_postdata( $post );
			
			$classes = array( 'wptwa-button', 'wptwa-account', 'wptwa-clearfix' );
			
			$from = get_post_meta( $post->ID, 'wptwa_hour_start', true ) . ':' . get_post_meta( $post->ID, 'wptwa_minute_start', true );
			$till = get_post_meta( $post->ID, 'wptwa_hour_end', true ) . ':' . get_post_meta( $post->ID, 'wptwa_minute_end', true );
			
			$offline_text = get_post_meta( $post->ID, 'wptwa_offline_text', true );
			
			/* Ignore if time is unavailable */
			if ( ! WPTWA_Display::isBetweenTime( $from, $till, current_time( 'H:i' ) ) ) {
				if ( '' === trim( $offline_text ) ) {
					continue;
				}
				else {
					$classes[] = 'wptwa-offline';
				}
			}
			
			/* Ignore if day is unavailable */
			if ( 'off' === get_post_meta( $post->ID, 'wptwa_' . strtolower( current_time( 'l' ) ), true ) ) {
				if ( '' === trim( $offline_text ) ) {
					continue;
				}
				else {
					$classes[] = 'wptwa-offline';
				}
			}
			
			$number = preg_replace( '/[^0-9]/', '', get_post_meta( $post->ID, 'wptwa_number', true ) );
			$name = get_post_meta( $post->ID, 'wptwa_name', true );
			$title = get_post_meta( $post->ID, 'wptwa_title', true );
			$title = '' !== $title ? ' &nbsp;/&nbsp; ' . $title : '';
			$button_label = get_post_meta( $post->ID, 'wptwa_button_label', true );
			$button_label = '' !== $button_label ? $button_label : WPTWA_Utils::getSetting( 'button_label', esc_html__( 'Need help? Chat via WhatsApp', 'wptwa' ) );
			$predefined_text = get_post_meta( $post->ID, 'wptwa_predefined_text', true );
			$predefined_text = str_ireplace( '[wptwa_page_title]', $page_title, $predefined_text );
			$predefined_text = str_ireplace( '[wptwa_page_url]', $page_url, $predefined_text );
			$predefined_text = str_ireplace( "\r\n", rawurlencode( "\r\n" ), $predefined_text );
			
			$post_title = get_the_title( $post );
			
			$avatar_url = '';
			if ( has_post_thumbnail( $post ) ) {
				$avatar = '<img src="' . get_the_post_thumbnail_url( $post ) . '" alt="' . $name . '"/>';
			}
			else {
				$avatar = '<svg class="WhatsApp" width="40px" height="40px" viewBox="0 0 92 92"><use xlink:href="#wptwa-logo"></svg>';
			}
			
			
			
			if ( 'on' === get_post_meta( $post->ID, 'wptwa_hide_on_large_screen', true ) ) {
				$classes[] = 'wptwa-hide-on-large-screen';
			}
			
			if ( 'on' === get_post_meta( $post->ID, 'wptwa_hide_on_small_screen', true ) ) {
				$classes[] = 'wptwa-hide-on-small-screen';
			}
			
			if ( 'round' === WPTWA_Utils::getSetting( 'button_style' ) ) {
				$classes['wptwa-round'] = 'wptwa-round';
			}
			
			if ( 'round' === strtolower( $button_style ) ) {
				$classes['wptwa-round'] = 'wptwa-round';
			}
			
			$href = 'https://api.whatsapp.com/send?phone=' . $number . ( '' !== $predefined_text ? '&text=' . $predefined_text : '' );
			if ( strpos( get_post_meta( $post->ID, 'wptwa_number', true ), 'chat.whatsapp.com' ) !== false ) {
				$number = '';
				$href = esc_url( get_post_meta( $post->ID, 'wptwa_number', true ) );
				$classes[] = 'wptwa-group';
			}
			
			$background_color = get_post_meta( $post->ID, 'wptwa_background_color', true );
			$background_color_on_hover = get_post_meta( $post->ID, 'wptwa_background_color_on_hover', true );
			$text_color = get_post_meta( $post->ID, 'wptwa_text_color', true );
			$text_color_on_hover = get_post_meta( $post->ID, 'wptwa_text_color_on_hover', true );
			
			if ( '' !== trim( $background_color ) 
					|| '' !== trim( $background_color_on_hover )
					|| '' !== trim( $text_color ) 
					|| '' !== trim( $text_color_on_hover )
				) {
				
				$item.= '<style type="text/css" scoped>';
				$item.= '#wptwa-button-' . $post->ID . ' {';
				$item.= ( '' !== trim( $background_color ) ) ? 'background-color:' . $background_color . ' !important;' : '';
				$item.= ( '' !== trim( $text_color ) ) ? 'color:' . $text_color . ' !important;' : '';
				$item.= '}';
				$item.= '#wptwa-button-' . $post->ID . ':hover {';
				$item.= ( '' !== trim( $background_color_on_hover ) ) ? 'background-color:' . $background_color_on_hover . ' !important;' : '';
				$item.= ( '' !== trim( $text_color_on_hover ) ) ? 'color:' . $text_color_on_hover . ' !important;' : '';
				$item.= '}';
				$item.= '</style>';
				
			}
			
			if ( in_array( 'wptwa-offline', $classes ) ) {
				$item.= '<span id="wptwa-button-' . $post->ID . '" class="' . implode( ' ', $classes) . '" >';
				$item.= '<span class="wptwa-avatar">' . $avatar . '</span><span class="wptwa-text"><span class="wptwa-profile">' . $name . $title . '</span><span class="wptwa-copy">' . $button_label . '</span><span class="wptwa-offline-text">' . $offline_text . '</span></span>';
				$item.= '</span>';
			}
			else {
				$item.= '<a href="' . $href . '" id="wptwa-button-' . $post->ID . '" class="' . implode( ' ', $classes) . '" data-number="' . $number . '" data-auto-text="' . esc_attr( $predefined_text ) . '" data-ga-label="' . esc_attr( $post_title ) . '" target="_blank">';
				$item.= '<span class="wptwa-avatar">' . $avatar . '</span><span class="wptwa-text"><span class="wptwa-profile">' . $name . $title . '</span><span class="wptwa-copy">' . $button_label . '</span></span>';
				$item.= '</a>';
			}
						
		}
		
		wp_reset_postdata();
		
		return wpautop( $item );
	}
	
	/* Legacy code. Delete it and it will break the old shortcode. */
	public function shortcodeWhatsApp ( $atts, $content = null ) {
		
		extract( shortcode_atts( array(
			'number' => '',
			'group_invite_url' => '',
			'auto_text' => '',
			'text_color' => '',
			'background_color' => '',
			'text_color_on_hover' => '',
			'background_color_on_hover' => '',
			'display' => 'inline-block',
			'icon' => 'yes',
			'align' => 'center'
		), $atts ) );
		
		if ( '' === trim( $number ) && ( '' === esc_url( $group_invite_url ) || strpos( $group_invite_url, 'chat.whatsapp.com' ) === false ) ) {
			return '';
		}
		
		$number = preg_replace( '/[^0-9]/', '', $number );
		
		$auto_text = '' !== trim( esc_attr( $auto_text ) ) ? esc_attr( $auto_text ) : '';
		$href = 'https://api.whatsapp.com/send?phone=' . $number . ( '' !== $auto_text ? '&text=' . $auto_text : '' );
		
		$button_id = 'whatsapp-button-' . self::$button_id++;
		$fa = '';
		$class = array( 'wptwa-account' );
		
		if ( '' !== esc_url( $group_invite_url ) && strpos( $group_invite_url, 'chat.whatsapp.com' ) !== false ) {
			$href = esc_url( $group_invite_url );
			$class[] = 'wptwa-group';
		}
		
		if ( '' !== $text_color ||
				'' !== $background_color ||
				'' !== $text_color_on_hover ||
				'' !== $background_color_on_hover ) {
			
			self::$styles .= '<style type="text/css">
				
				#' . $button_id . ' {
					color: ' . $text_color . ';
					background: ' . $background_color . ';
					
				}
				#' . $button_id . ' svg {
					fill: ' . $text_color . ';
				}
				#' . $button_id . ':hover {
					color: ' . $text_color_on_hover . ';
					background: ' . $background_color_on_hover . ';
					text-decoration: none;
				}
				#' . $button_id . ':hover svg {
					fill: ' . $text_color_on_hover . ';
				}
				
				</style>';
			
			$fa = '<svg class="WhatsApp" width="15px" height="15px" viewBox="0 0 90 90"><use xlink:href="#wptwa-logo"></svg>';
				
			$class[] = 'whatsapp-custom-styled';
			
			if ( 'block' === $display ) {
				$class[] = 'block-level';
			}
			if ( 'yes' !== $icon ) {
				$class[] = 'no-icon';
			}
			if ( 'left' === $align ) {
				$class[] = 'align-left';
			}
		}
		
		if ( 'block' === $display ) {
			return  wpautop( '<a id="' . $button_id . '" class="' . implode( ' ', $class ) . '" href="' . $href . '" data-number="' . $number . '" data-auto-text="' . $auto_text . '" target="_blank">' . $fa . ( '' !== $content ? '<span>' . $content . '</span>' : '' ) . '</a>' );
		}
		else {
			return  '<a id="' . $button_id . '" class="' . implode( ' ', $class ) . '" href="' . $href . '" data-number="' . $number . '" data-auto-text="' . $auto_text . '" target="_blank">' . $fa . ( '' !== $content ? '<span>' . $content . '</span>' : '' ) . '</a>';
		}
		
	}
	
	public static function addStyles () {
		
		echo self::$styles;
		
	}
	
}

?>