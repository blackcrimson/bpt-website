=== WooCommerce Virtual Card Services Gateway ===

A payment gateway for Virtual Card Services. A VCS terminal ID is required for this gateway to function.

== Important Note ==

An SSL certificate is recommended for additional safety and security for your customers.