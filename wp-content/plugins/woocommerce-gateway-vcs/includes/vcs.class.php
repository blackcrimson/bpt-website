<?php
/**
 * Virtual Card Services Payment Gateway
 *
 * Provides a Virtual Card Services Payment Gateway.
 *
 * @class 		woocommerce_gateway_vcs
 * @package		WooCommerce
 * @category	Payment Gateways
 * @author		WooThemes
 *
 *
 * Table Of Contents
 *
 * __construct()
 * init_form_fields()
 * plugin_url()
 * add_currency()
 * add_currency_symbol()
 * is_valid_for_use()
 * get_country_code()
 * admin_options()
 * payment_fields()
 * generate_vcs_form()
 * process_payment()
 * receipt_page()
 * prepare_form_fields()
 * check_response_is_valid()
 * check_response()
 * successful_request()
 * log()
 * amounts_equal()
 * add_gateway()
 * ssl_check()
 */
class WC_Gateway_VCS extends WC_Payment_Gateway {

	public function __construct() {

        $this->id			= 'vcs';
        $this->method_title = __( 'Virtual Card Services', 'wc_vcs' );
        $this->icon 		= apply_filters( 'woocommerce_vcs_icon', $this->plugin_url() . '/assets/images/icon.png' );
        $this->has_fields 	= true;
        $this->debug_email 	= get_option( 'admin_email' );
        $this->wc_version 	= get_option( 'woocommerce_db_version' );

		// Setup available countries.
		$this->available_countries = array( 'ZA' );

		// Setup available currency codes.
		$this->available_currencies = array( 'ZAR' );

		// Load the form fields.
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();

		// Setup default merchant data.
		// $this->url = 'https://www.vcs.co.za/vvonline/ccform.asp';
		$this->url = 'https://www.vcs.co.za/vvonline/vcspay.aspx';
		$this->title = $this->settings['title'];

		// Active logs.
		if ( 'yes' === $this->settings['testmode'] ) {
			if ( class_exists( 'WC_Logger' ) ) {
				$this->log = new WC_Logger();
			} else {
				$this->log = WC()->logger();
			}
		}

		// revert to non api for wc < 2 as VCS does not support ? in return url
		if ( version_compare( $this->wc_version, '2.0', '<' ) ) {
			add_action( 'init', array( $this, 'check_response' ) );
			$this->callback_url = home_url( '/' );
		} else {
			add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'check_response') );
			$this->callback_url = WC()->api_request_url( get_class( $this ) );
		}
		add_action( 'valid-vcs-response', array( $this, 'successful_request' ) );

		add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

		add_action( 'woocommerce_receipt_vcs', array( $this, 'receipt_page' ) );

		// Check if the base currency supports this gateway.
		if ( ! $this->is_valid_for_use() ) { $this->enabled = false; }
    }

	/**
     * Initialise Gateway Settings Form Fields
     *
     * @since 1.0.0
     */
    function init_form_fields () {

    	$this->form_fields = array(
							'enabled' => array(
											'title' => __( 'Enable/Disable', 'wc_vcs' ),
											'label' => __( 'Enable VCS', 'wc_vcs' ),
											'type' => 'checkbox',
											'description' => __( 'This controls whether or not this gateway is enabled within WooCommerce.', 'wc_vcs' ),
											'default' => 'yes'
										),
							'title' => array(
    										'title' => __( 'Title', 'wc_vcs' ),
    										'type' => 'text',
    										'description' => __( 'This controls the title which the user sees during checkout.', 'wc_vcs' ),
    										'default' => __( 'Virtual Card Services', 'wc_vcs' )
    									),
							'description' => array(
											'title' => __( 'Description', 'wc_vcs' ),
											'type' => 'text',
											'description' => __( 'This controls the description which the user sees during checkout.', 'wc_vcs' ),
											'default' => __( 'Pay using the Virtual Card Services terminal.', 'wc_vcs' )
										),
							'terminal_id' => array(
											'title' => __( 'Terminal ID', 'wc_vcs' ),
											'type' => 'text',
											'description' => __( 'This is the terminal ID, received from VCS.', 'wc_vcs' ),
											'default' => ''
										),
							'pam' => array(
											'title' => __( 'Personal Authentication Message', 'wc_vcs' ),
											'type' => 'text',
											'description' => __( 'This is the Personal Authentication Message (PAM), added in your VCS virtual terminal settings.', 'wc_vcs' ),
											'default' => sprintf( __( 'Thank you for shopping at %s', 'wc_vcs' ), get_bloginfo( 'name' ) )
										),
							'testmode' => array(
											'title' => __( 'Test Mode', 'wc_vcs' ),
											'type' => 'checkbox',
											'label' => __( 'Place the payment gateway in development mode.', 'wc_vcs' ),
											'default' => 'yes'
										)
							);

    } // End init_form_fields()

    /**
	 * Get the plugin URL
	 *
	 * @since 1.0.0
	 */
	function plugin_url() {
		if( isset( $this->plugin_url ) ) return $this->plugin_url;

		if ( is_ssl() ) {
			return $this->plugin_url = str_replace('http://', 'https://', WP_PLUGIN_URL) . "/" . plugin_basename( dirname(dirname(__FILE__)));
		} else {
			return $this->plugin_url = WP_PLUGIN_URL . "/" . plugin_basename( dirname(dirname(__FILE__)));
		}
	} // End plugin_url()

    /**
     * is_valid_for_use()
     *
     * Check if this gateway is enabled and available in the base currency being traded with.
     *
     * @since 1.0.0
     */
	function is_valid_for_use() {

		$is_available = false;

        $user_currency = get_option( 'woocommerce_currency' );

        $is_available_currency = in_array( $user_currency, $this->available_currencies );


		if ( $is_available_currency && $this->enabled == 'yes' && $this->settings['terminal_id'] != '' ) { $is_available = true; }

        return $is_available;
	} // End is_valid_for_use()

	/**
	 * get_country_code()
	 *
     * Get the users country either from their order, or from their customer data
     *
     * @since 1.0.0
     */
	function get_country_code() {

		$base_country = WC()->countries->get_base_country();

		return $base_country;
	} // End get_country_code()

	/**
	 * Admin Panel Options
	 * - Options for bits like 'title' and availability on a country-by-country basis
	 *
	 * @since 1.0.0
	 */
	public function admin_options() {
    	?>
    	<h3><?php _e( 'Virtual Card Services', 'wc_vcs' ); ?></h3>
    	<p><?php printf( __( 'The Virtual Card Services payment gateway works by directing the visitor to %sVirtual Card Services%s to enter their payment information.', 'wc_vcs' ), '<a href="http://www.vcs.co.za/">', '</a>' ); ?></p>

    	<?php
    	if ( 'ZAR' == get_option( 'woocommerce_currency' ) ) {
    	?>
    	<table class="form-table"><?php
		// Generate the HTML For the settings form.
		$this->generate_settings_html();
		?><tr valign="top">
			<td colspan="2">

			</td>
		</tr>
		</table><!--/.form-table-->
		<!--Virtual Terminl Setup -->
		<div>
			<h3><?php _e( 'Virtual Terminal Setup', 'wc_vcs' );?></h3>
			<p>
				<?php _e('Copy the following URL to the field labeled <strong>Website URL</strong>','wc_vcs' );?>:&nbsp;&nbsp;<strong><?php echo $this->callback_url; ?></strong><br/>
				<?php _e('Copy the following URL to the field labeled <strong>Approved page URL</strong>','wc_vcs' );?>:&nbsp;&nbsp;<strong><?php echo $this->callback_url; ?></strong><br/>
				<?php _e('Copy the following URL to the field labeled <strong>Declined page URL</strong>','wc_vcs' );?>:&nbsp;&nbsp;<strong><?php echo $this->callback_url; ?></strong><br/>
			</p>

		</div>
    	<?php
		} else {
		?>
			<div class="inline error"><p><strong><?php _e( 'Gateway Disabled', 'wc_vcs' ); ?></strong> <?php echo sprintf( __( 'Choose South African Rands as your store currency in <a href="%s">Pricing Options</a> to enable the VCS Gateway.', 'woocommerce' ), admin_url( 'admin.php?page=wc-settings&tab=general' ) ); ?></p></div>
		<?php
		} // End check currency
		?>
    	<?php
    } // End admin_options()

    /**
	 * There are no payment fields for VCS, but we want to show the description if set.
	 *
	 * @since 1.0.0
	 */
    function payment_fields() {
    	$user_country = $this->get_country_code();

		if ( empty( $user_country ) ) {
			_e( 'Please complete your billing information before entering payment details.', 'wc_vcs' );
			return;
		}

    	if ( isset( $this->settings['description'] ) && ( '' != $this->settings['description'] ) ) {
    		echo wpautop( wptexturize( $this->settings['description'] ) );
    	}
    } // End payment_fields()

	/**
	 * Generate the Virtual Card Services button link.
	 *
	 * @since 1.0.0
	 */

    public function generate_vcs_form( $order_id ) {

		$order = new WC_Order( $order_id );

		$fields = $this->prepare_form_fields( $order );

		$vcs_args_array = array();

		foreach ( $fields as $key => $value ) {
			$vcs_args_array[] = '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
		}

		return '<form action="' . $this->url . '" method="post" id="vcs_payment_form">
				' . implode( '', $vcs_args_array ) . '
				<input type="submit" class="button-alt" id="submit_vcs_payment_form" value="' . __( 'Pay via Virtual Card Services', 'wc_vcs' ) . '" /> <a class="button cancel" href="' . $order->get_cancel_order_url() . '">' . __( 'Cancel order &amp; restore cart', 'wc_vcs' ) . '</a>
				<script type="text/javascript">
					jQuery(function(){
						jQuery("body").block(
							{
								message: "<img src=\"' . WC()->plugin_url() . '/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" />' . __( 'Thank you for your order. We are now redirecting you to Virtual Card Services to make payment.', 'wc_vcs' ) . '",
								overlayCSS:
								{
									background: "#fff",
									opacity: 0.6
								},
								css: {
							        padding:        20,
							        textAlign:      "center",
							        color:          "#555",
							        border:         "3px solid #aaa",
							        backgroundColor:"#fff",
							        cursor:         "wait"
							    }
							});
						jQuery( "#submit_vcs_payment_form" ).click();
					});
				</script>
			</form>';

		$order->add_order_note( __( 'Customer was redirected to VCS.', 'wc_vcs' ) );
	} // End generate_vcs_form()

	/**
	 * Process the payment and return the result.
	 *
	 * @since 1.0.0
	 */
	function process_payment( $order_id ) {

		$order = new WC_Order( $order_id );

		return array(
			'result' 	=> 'success',
			'redirect'	=> $order->get_checkout_payment_url( true )
		);

	}

	/**
	 * Receipt page.
	 *
	 * Display text and a button to direct the user to the payment screen.
	 *
	 * @since 1.0.0
	 */
	function receipt_page( $order ) {
		echo '<p>' . __( 'Thank you for your order, please click the button below to pay with Virtual Card Services.', 'wc_vcs' ) . '</p>';

		echo $this->generate_vcs_form( $order );
	} // End receipt_page()

	/**
     * prepare_form_fields()
     *
     * Prepare the fields to be submitted to Virtual Card Services.
     *
     * @param object $order
     * @return array
     */
	function prepare_form_fields( $order ) {

		$amount     = $order->get_total();
		$currency   = get_option( 'woocommerce_currency' );
		$order_no   = $order->get_order_number();
		$order_id   = version_compare( WC_VERSION, '3.0', '<' ) ? $order->id : $order->get_id();

		// this is a hacky work around as VCS rejects orders that have
		// previously been logged with same order number that causes a loop
		// when payment fails the first time around due to invalid card # or expiry date
		if ( $this->has_failed( $order ) ) {

			$attempts = intval( get_post_meta( $order_id, '_wc_vcs_failed_attempts', true ) );
			// increment attempt as this is a retry
			if ( $attempts > 0   ) {
				$attempts ++;
			} else {
				// initialise attempts as this is the first try
				$attempts = 1;
			}
			$order_no = $order_no . '-' . $attempts;
			update_post_meta( $order_id, '_wc_vcs_failed_attempts', $attempts );

		}

        $params = array(
            'p1'        =>  $this->settings['terminal_id'],
            'p2'        =>  $order_no,
            'p3'        =>  sprintf( __( '%s purchase, Order # %d', 'wc_vcs' ), wp_specialchars_decode( get_bloginfo( 'name' ), ENT_QUOTES ), $order->get_order_number() ),
            'p4'        =>  $amount,
            'p5'        =>  $currency,
            'p10' 		=> 	$order->get_cancel_order_url(), // The URL to direct to when the customer clicks "Cancel" on VCS.
            'm_3'       =>  $order_id, // This is returned to us when returning from VCS.
        );

        if ( $this->settings['pam'] != '' ) {
            $params['m_1'] = md5( $this->settings['pam'] . '::' . $params['p2'] );
        }

        $params['m_2'] = version_compare( WC_VERSION, '3.0', '<' ) ? $order->order_key : $order->get_order_key(); // This is returned to us when returning from VCS.

    	return $params;
    } // End prepare_form_fields()

    /**
     * Check if order has previously failed
     *
     * @param object $order
     * @return bool
     */
    public function has_failed( $order ) {
    	if ( 'failed' === $order->get_status() ) {
    		return true;
    	}

    	return false;
    }

	/**
	 * Check VCS response validity.
	 *
	 * @param array $data
	 * @since 1.0.0
	 */
	function check_response_is_valid( $data ) {

		$has_error = false;
		$error_message = '';
		$is_done = false;

		$order_key = esc_attr( $data['m_2'] );
		$order = new WC_Order( $data['m_3'] );

		$data_string = '';

		$this->log( "\n" . '----------' . "\n" . 'VCS response received' );

		// check transaction password
        if ( $this->settings['pam'] != '' && ! $has_error && ! $is_done ) {
            if ( $this->settings['pam'] != $data['pam'] ) {
                $has_error = true;

                $error_message = 'Transaction password incorrect. Please ensure your Personal Authentication Message in WooCommerce settings the setting your VCS terminal.';
                $this->log( $error_message );
            }
            if ( $data['m_1'] != md5( $data['pam'] . '::' . $data['p2'] ) && ! $has_error && ! $is_done ) {
            	$has_error = true;

            	$error_message = 'Checksum mismatch.';
            	$this->log( $error_message );
            }
        }

		// check transaction status
		if ( ! empty( $data['p3'] ) && substr( $data ['p3'], 6, 8 ) != 'APPROVED' && ! $has_error && ! $is_done ) {
			$has_error = true;

			$error_message = 'Transaction was not successful.';
			$this->log( $error_message );

			$order->update_status( 'failed', sprintf( __( 'Payment failed via VCS. Response: %s', 'wc_vcs' ), esc_attr( $data['p3'] ) ) );
			wp_redirect( $this->get_return_url( $order ) );
			exit;
		}

        // Get data sent by the gateway
        if ( ! $has_error && ! $is_done ) {
        	$this->log( 'Get posted data' );

            $this->log( 'VCS Data: '. print_r( $data, true ) );

            if ( $data === false ) {
                $has_error = true;
                $error_message = 'Bad access on page.';
            }
        }

        // Get internal order and verify it hasn't already been processed
        if( ! $has_error && ! $is_done ) {

            $this->log( "Purchase:\n". print_r( $order, true )  );

            // Check if order has already been processed
            if ( 'completed' === $order->get_status() ) {
                $this->log( 'Order has already been processed' );
                $is_done = true;
            }
        }

        // Check data against internal order
        if( ! $has_error && ! $is_done ) {
            $this->log( 'Check data against internal order' );
            $this->log( 'Total from VCS: ' . $data['p6'] );
            $this->log( 'Total stored internally: ' . $order->get_total() );

            // Check order amount
            if( ! $this->amounts_equal( $data['p6'], $order->get_total() ) ) {
                $has_error = true;
                $error_message = 'Order totals don\'t match.';
            }
            // Check session ID
            elseif ( $data['m_2'] != ( version_compare( WC_VERSION, '3.0', '<' ) ? $order->order_key : $order->get_order_key() ) ) {
                $has_error = true;
                $error_message = 'Order key mismatch.';
            }
        }

        // If an error occurred
        if( $has_error ) {
            $this->log( 'Error occurred: ' . $error_message );
            $is_done = false;
        } else {
        	$this->log( 'Transaction completed.' );
        	$is_done = true;

        	// Payment completed
            $order->payment_complete();
			$order->add_order_note( sprintf( __( 'Payment via VCS completed. Response: %s', 'wc_vcs' ), $posted['p3'] ) );

			// Empty the Cart
			WC()->cart->empty_cart();
        }

    	return $is_done;
    } // End check_response_is_valid()

	/**
	 * Check gateway response.
	 *
	 * @since 1.0.0
	 */
	function check_response() {
		// Clean
		@ob_clean();

		// Header
		header( 'HTTP/1.1 200 OK' );

		if ( ! empty( $_POST ) && isset( $_POST['p2'] ) ) {
			$_POST = stripslashes_deep( $_POST );

			if ( $this->check_response_is_valid( $_POST ) ) {
				do_action( 'valid-vcs-response', sanitize_text_field( $_POST['p2'] ) );
			}

		}
	} // End check_response()

	/**
	 * Successful Payment!
	 *
	 * @since 1.0.0
	 */
	function successful_request( $order_id = 0 ) {

		if ( ! $order_id ) {
			return false;
		}

		// data at m_3 was sent as the original order id
		// ignoring sequential order numbers and retry attempts
		if ( ! empty( $_POST['m_3'] ) ) {
			$order_id =  $_POST['m_3'];
		}

		$order = wc_get_order( $order_id );

		wp_redirect( $this->get_return_url( $order ) );
		exit;

	} // End successful_request()

	/**
	 * log()
	 *
	 * Log system processes.
	 *
	 * @since 1.0.0
	 */

	function log( $message ) {
		if ( 'yes' === $this->settings['testmode'] ) {
			$this->log->add( $this->id, $message );
		}
	} // End log()

	/**
	 * amounts_equal()
	 *
	 * Checks to see whether the given amounts are equal using a proper floating
	 * point comparison with an Epsilon which ensures that insignificant decimal
	 * places are ignored in the comparison.
	 *
	 * eg. 100.00 is equal to 100.0001
	 *
	 * @author Jonathan Smit
	 * @param $amount1 Float 1st amount for comparison
	 * @param $amount2 Float 2nd amount for comparison
	 * @since 1.0.0
	 */
	function amounts_equal ( $amount1, $amount2 ) {
	    if( abs( floatval( $amount1 ) - floatval( $amount2 ) ) > 0.01 )
	        return( false );
	    else
	        return( true );
	} // End amounts_equal()

} // End Class
?>
