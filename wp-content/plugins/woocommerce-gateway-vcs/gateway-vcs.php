<?php
/*
 * Plugin Name: WooCommerce Virtual Card Services Gateway
 * Plugin URI: https://woocommerce.com/products/virtual-card-services/
 * Description: A payment gateway for South African payment system, Virtual Card Services.
 * Version: 1.1.10
 * Author: WooCommerce
 * Author URI: https://woocommerce.com/
 * Woo: 18600:f4cd4a5f87f2446ce9dbb82f1504f161
 * WC tested up to: 3.5
 * WC requires at least: 2.6
 * Requires at least: 3.1
 * Tested up to: 3.2.1
 */

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'f4cd4a5f87f2446ce9dbb82f1504f161', '18600' );

load_plugin_textdomain( 'wc_vcs', false, trailingslashit( dirname( plugin_basename( __FILE__ ) ) ) );

add_action( 'plugins_loaded', 'woocommerce_vcs_init', 0 );

function woocommerce_vcs_init () {
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) { return; }
	require_once( plugin_basename( 'includes/vcs.class.php' ) );
	add_filter( 'woocommerce_payment_gateways', 'woocommerce_vcs_add_gateway' );
} // End woocommerce_vcs_init()

function woocommerce_vcs_add_gateway( $methods ) {
	$methods[] = 'WC_Gateway_VCS'; return $methods;
} // End woocommerce_evripay_add_gateway()
?>
