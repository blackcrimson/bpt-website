<?php
namespace sgpb;
require_once(dirname(__FILE__).'/SGPopup.php');
class IframePopup extends SGPopup
{
	public function __construct()
	{
		add_filter('sgpbAdminJsFiles', array($this, 'adminJsFilter'), 1, 1);
		add_filter('sgpbPopupDefaultOptions', array($this, 'filterPopupDefaultOptions'));
	}

	private function frontendFilters()
	{
		add_filter('sgpbFrontendJsFiles', array($this, 'popupFrontJsFilter'), 1, 1);
	}

	public function filterPopupDefaultOptions($defaultOptions)
	{
		$changingOptions = array(
			'sgpb-content-padding' => array('name' => 'sgpb-content-padding', 'type' => 'text', 'defaultValue' => 0),
			'sgpb-width' => array('name' => 'sgpb-width', 'type' => 'text', 'defaultValue' => '60%'),
			'sgpb-height' => array('name' => 'sgpb-height', 'type' => 'text', 'defaultValue' => '60%')
		);

		$defaultOptions = $this->changeDefaultOptionsByNames($defaultOptions, $changingOptions);

		return $defaultOptions;
	}

	public function popupFrontJsFilter($jsFiles)
	{
		$jsFiles[] = array('folderUrl'=> SG_POPUP_JS_URL, 'filename' => 'Video.js', 'dep' => array('PopupBuilder.js'));

		return $jsFiles;
	}

	public function adminJsFilter($jsFiles)
	{
		$jsFiles[] = array('folderUrl' => SG_POPUP_JS_URL, 'filename' => 'Iframe.js');

		return $jsFiles;
	}

	public function getOptionValue($optionName, $forceDefaultValue = false)
	{
		return parent::getOptionValue($optionName, $forceDefaultValue);
	}

	public function getPopupTypeMainView()
	{
		return array(
			'filePath' => SG_POPUP_TYPE_MAIN_PATH.'iframe.php',
			'metaboxTitle' => 'General'
		);
	}

	private function getIframeTag()
	{
		$options = $this->getOptions();
		$iframeScroll = '';
		$isMobile = AdminHelper::isAppleMobileDevice();
		if ($isMobile) {
			$iframeScroll = 'scrolling="no"';
		}
		$id = $this->getId();
		$iframeUrl = $options['sgpb-iframe-url'];

		// Iframe wrapped to div for fix mobile issue
		$iframe = '<div class="sgpb-scroll-wrapper">';
		// Added iframe random name for iframe must be have one name
		$iframe .= '<iframe allowfullscreen src="" data-attr-src="'.$iframeUrl.'" class="sgpb-iframe-spiner sgpb-iframe-'.$id.'" name="1507897640139" '.$iframeScroll.'></iframe>';
		$iframe .= '</div>';

		return $iframe;
	}

	public function getPopupTypeContent()
	{
		$this->frontendFilters();
		$popupContent = $this->getContent();
		$iframeTag = $this->getIframeTag();
		$popupContent .= $iframeTag;
		$popupContent .= '<style>';
		$popupContent .= '.sgpb-popup-builder-content-html {';
		$popupContent .= 'width: 100%;';
		$popupContent .= 'height: 100%;';
		$popupContent .= 'overflow: auto';
		$popupContent .= '}';
		$popupContent .= '</style>';

		return $popupContent;
	}

	public function getRemoveOptions()
	{
		// Where 1 mean this options must not show for this popup type
		$removeOptions = array(
			'sgpb-content-click' => 1,
			'sgpb-popup-dimension-mode' => 1,
			'sgpb-force-rtl' => 1
		);

		return $removeOptions;
	}

	/**
	 * It returns what the current post supports (for example: title, editor, etc...)
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public static function getPopupTypeSupports()
	{
		return array('title');
	}

	public function getExtraRenderOptions()
	{
		$options = $this->getOptions();

		return $options;
	}
}
