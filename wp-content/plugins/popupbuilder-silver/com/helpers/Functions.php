<?php
namespace sgpb;
use \SxGeo;

class Functions
{
	public static function getIpAddress()
	{
		if (getenv('HTTP_CLIENT_IP'))
			$ipAddress = getenv('HTTP_CLIENT_IP');
		else if (getenv('HTTP_X_FORWARDED_FOR'))
			$ipAddress = getenv('HTTP_X_FORWARDED_FOR');
		else if (getenv('HTTP_X_FORWARDED'))
			$ipAddress = getenv('HTTP_X_FORWARDED');
		else if (getenv('HTTP_FORWARDED_FOR'))
			$ipAddress = getenv('HTTP_FORWARDED_FOR');
		else if (getenv('HTTP_FORWARDED'))
			$ipAddress = getenv('HTTP_FORWARDED');
		else if (getenv('REMOTE_ADDR'))
			$ipAddress = getenv('REMOTE_ADDR');
		else
			$ipAddress = 'UNKNOWN';

		return $ipAddress;
	}

	// proStartPlatinum
	public static function getCountryName($ip)
	{
		if (empty($_COOKIE['SG_POPUP_USER_COUNTRY_NAME'])) {
			require_once(SG_POPUP_LIBS_PATH.'SxGeo/SxGeo.php');

			$SxGeo = new SxGeo(SG_POPUP_LIBS_PATH.'SxGeo/SxGeo.dat');
			$country = $SxGeo->getCountry($ip);
			if ($country) {
				setcookie('SG_POPUP_USER_COUNTRY_NAME', $country, strtotime('+365 days'), "/");
			}
		}
		else {
			$country = $_COOKIE['SG_POPUP_USER_COUNTRY_NAME'];
		}

		return $country;
	}
	// proEndPlatinum

	// proStartSilver
	public static function getUserDevice()
	{
		$deviceName = 'Not found any device';
		if (empty($_SERVER["HTTP_USER_AGENT"])) {
			return $deviceName;
		}

		$userAgent = $_SERVER["HTTP_USER_AGENT"];
		$devicesTypes = array(
			"is_desktop" => array("msie 10", "msie 9", "msie 8", "windows.*firefox", "windows.*chrome", "x11.*chrome", "x11.*firefox", "macintosh.*chrome", "macintosh.*firefox", "opera"),
			"is_tablet"   => array("tablet", "android", "ipad", "tablet.*firefox"),
			"is_mobile"   => array("mobile ", "android.*mobile", "iphone", "ipod", "opera mobi", "opera mini"),
			"is_bot"      => array("googlebot", "mediapartners-google", "adsbot-google", "duckduckbot", "msnbot", "bingbot", "ask", "facebook", "yahoo", "addthis")
		);

		foreach ($devicesTypes as $deviceType => $devices) {
			foreach ($devices as $device) {
				if (preg_match('/'.$device.'/i', $userAgent)) {
					$deviceName = $deviceType;
				}
			}
		}

		return $deviceName;
	}
	// proEndSilver

	public static function renderForm($formFields)
	{
		$form = '';

		if (empty($formFields) || !is_array($formFields)) {
			return $form;
		}
		$simpleElements = array(
			'text',
			'email',
			'hidden',
			'submit',
			'button'
		);

		$form = '<form class="sgpb-form" id="sgpb-form" method="post">';
		$fields = '<div class="sgpb-form-wrapper">';
		foreach ($formFields as $fieldKey => $formField) {
			$htmlElement = '';
			$hideClassName = '';
			$type = 'text';

			if (!empty($formField['attrs']['type'])) {
				$type = $formField['attrs']['type'];
			}

			$styles = '';
			$attrs = '';
			$label = '';
			$gdprWrapperStyles = '';
			$gdprText = '';
			$errorMessageBoxStyles = '';
			$errorWrapperClassName = @$formField['attrs']['name'].'-error-message';
			if (isset($formField['errorMessageBoxStyles'])) {
				$errorMessageBoxStyles = 'style="width:'.$formField['errorMessageBoxStyles'].'"';
			}
			if (!empty($formField['label'])) {
				$label = $formField['label'];
				if (isset($formField['text'])) {
					$gdprText = $formField['text'];
				}
				$formField['style'] = array('color' => @$formField['style']['color'], 'width' => $formField['style']['width']);
				$gdprWrapperStyles = 'style="color:'.$formField['style']['color'].'"';
			}

			if ($type == 'checkbox') {
				$formField['style']['max-width'] = $formField['style']['width'];
				unset($formField['style']['width']);
			}
			if (!empty($formField['style'])) {
				$styles = 'style="';
				if (strpos(@$formField['attrs']['name'], 'gdpr') !== false) {
					unset($formField['style']['height']);
				}
				foreach ($formField['style'] as $styleKey => $styleValue) {
					if ($styleKey == 'placeholder') {
						$styles .= '';
					}
					$styles .= $styleKey.':'.$styleValue.'; ';
				}
				$styles .= '"';
			}

			if (!empty($formField['attrs'])) {
				foreach ($formField['attrs'] as $attrKey => $attrValue) {
					$attrs .= $attrKey.' = "'.esc_attr($attrValue).'" ';
				}
			}

			if (!$formField['isShow']) {
				$hideClassName = 'sg-js-hide';
			}

			if (in_array($type, $simpleElements)) {
				$htmlElement = self::createInputElement($attrs, $styles, $errorWrapperClassName, $errorMessageBoxStyles);
			}
			else if ($type == 'checkbox') {
				$htmlElement = self::createCheckbox($attrs, $styles);
				if (strpos(@$formField['attrs']['name'], 'gdpr') !== false) {
					$label = $formField['label'];
					if (isset($formField['text'])) {
						$gdprText = $formField['text'];
					}
					$formField['style'] = array('color' => @$formField['style']['color'], 'width' => @$formField['style']['width']);
					$gdprWrapperStyles = 'style="color:'.@$formField['style']['color'].'"';
					$htmlElement = self::createGdprCheckbox($attrs, $styles, $label, $gdprWrapperStyles, $gdprText);
				}
			}
			else if ($type == 'textarea') {
				$htmlElement = self::createTextArea($attrs, $styles, $errorWrapperClassName);
			}

			ob_start();
			?>
			<div class="sgpb-inputs-wrapper js-<?php echo $fieldKey; ?>-wrapper js-sgpb-form-field-<?php echo $fieldKey; ?>-wrapper <?php echo $hideClassName; ?>">
				<?php echo $htmlElement; ?>
			</div>
			<?php
			$fields .= ob_get_contents();
			ob_get_clean();
		}
		$fields .= '</div>';

		$form .= $fields;
		$form .= '</form>';

		return $form;
	}

	public static function createInputElement($attrs, $styles = '', $errorWrapperClassName = '')
	{
		$inputElement = "<input $attrs $styles>";

		if (!empty($errorWrapperClassName)) {
			$inputElement .= "<div class='$errorWrapperClassName'></div>";
		}

		return $inputElement;
	}

	public static function createCheckbox($attrs, $styles)
	{
		$inputElement = "<input $attrs $styles>";

		return $inputElement;
	}

	public static function createGdprCheckbox($attrs, $styles, $label = '', $gdprWrapperStyles = '', $text = '')
	{
		$inputElement = "<input $attrs>";
		$inputElement = '<div class="sgpb-gdpr-label-wrapper" '.$styles.'>'.$inputElement.'<label for="sgpb-gdpr-field-label">'.$label.'</label><div class="sgpb-gdpr-error-message"></div></div>';
		if ($text == '') {
			return $inputElement;
		}
		$inputElement .= '<div class="sgpb-alert-info sgpb-alert sgpb-gdpr-info js-subs-text-checkbox sgpb-gdpr-text-js" '.$styles.'>'.$text.'</div>';

		return $inputElement;
	}

	public static function createTextArea($attrs, $styles, $errorWrapperClassName = '')
	{
		$inputElement = "<textarea $attrs $styles></textarea>";
		if (!empty($errorWrapperClassName)) {
			$inputElement .= "<div class='$errorWrapperClassName'></div>";
		}

		return $inputElement;
	}

	public static function getPopupTypeToAllowToShowMetabox()
	{
		global $post;

		if ($post->post_type != SG_POPUP_POST_TYPE) {
			return false;
		}
		if (!empty($_GET['sgpb_type'])) {
			$type = $_GET['sgpb_type'];
		}
		else {
			$popupId = $post->ID;
			$popup = SGPopup::find($popupId);
			if (empty($popup) || !is_object($popup)) {
				return false;
			}
			$type = $popup->getType();
		}

		return $type;
	}

	public static function getExtensionDirectory($extensionFolderName = '', $popupMainFolderName = '')
	{
		$directoryExisits = is_dir(SGPB_POPUP_EXTENSIONS_PATH.$extensionFolderName);
		$dir = SGPB_POPUP_EXTENSIONS_PATH.$extensionFolderName;

		if (!$directoryExisits) {
			$directoryExisits = is_dir(WP_PLUGIN_DIR.'/'.$extensionFolderName);
			$dir = WP_PLUGIN_DIR.'/'.$extensionFolderName;
			if (!$directoryExisits) {
				$dir = false;
			}
		}

		return $dir;
	}
}
