SgpbEventListener.prototype.sgpbInactivity = function(listenerObj, eventData)
{
	var that = this;
	var popupId = 1;
	var timeout = parseInt(eventData.value);
	timeout = timeout*1000;

	var idleInterval = setInterval(function() {listenerObj.timerIncrement(listenerObj, idleInterval) }, timeout);

	jQuery(window).mousemove(function(e) {
		SgpbEventListener.inactivityIdicator++;
	});
	jQuery(window).keypress(function(e) {
		SgpbEventListener.inactivityIdicator++;
	});

	window.addEventListener("touchstart", handlerFunction, false);
	function handlerFunction(event) {
		SgpbEventListener.inactivityIdicator++;
	}
};

SGPBPopup.prototype.allowAfterXPages = function()
{
	var popupData = this.popupData;
	var popupId = this.id;
	var afterPagesLimit = popupData['sgpb-show-popup-after-x-pages-count'];
	var afterXPagesStatus = popupData['sgpb-show-popup-after-x-pages'];
	if (afterPagesLimit) {
		this.setVisitedPageByPopupId(popupId);
	}
	var visitedPages = this.getVisitedPageByPopupId(popupId);

	if (!afterXPagesStatus) {
		return true;
	}

	return visitedPages.length > afterPagesLimit;
};

SGPBPopup.prototype.getVisitedPageByPopupId = function(popupId)
{
	var visitedPagesCookie = SGPopup.getCookie('sgpbVisitedPages');
	var visitedPages = [];

	if (!visitedPagesCookie) {
		return visitedPages;
	}
	visitedPagesCookie = jQuery.parseJSON(visitedPagesCookie);

	return visitedPagesCookie[popupId];
};

SGPBPopup.prototype.setVisitedPageByPopupId = function(popupId)
{
	var visitedPagesCookie = SGPopup.getCookie('sgpbVisitedPages');
	var visitedPages = {};
	var currentUrl = window.location.href;

	if (visitedPagesCookie) {
		visitedPages = jQuery.parseJSON(visitedPagesCookie);
	}

	var popupVisitedPages = visitedPages[popupId] || [];

	if (popupVisitedPages.indexOf(currentUrl) == '-1') {
		popupVisitedPages.push(currentUrl);
	}

	visitedPages[popupId] = popupVisitedPages;
	visitedPages = JSON.stringify(visitedPages);

	SGPBPopup.setCookie('sgpbVisitedPages', visitedPages, 365, false);
};

function SGPBPopupPro()
{

}

SGPBPopupPro.prototype.eventListener = function()
{
	sgAddEvent(window, 'sgpbDidOpen', function(e) {
		var args = e.detail;
		var popupId = parseInt(args.popupId);

		var iframeTag = jQuery('.sgpb-popup-builder-content-'+popupId+' .sgpb-iframe-'+popupId);
		if (iframeTag.length) {
			iframeTag.load(function() {
				var popupData = SGPBPopup.getPopupWindowDataById(popupId);
				/* remove spinner class when popup is open*/
				if (typeof popupData != 'undefined' && popupData.isOpen) {
					jQuery(this).removeClass('sgpb-iframe-spiner');
				}
			});
		}
	});

	sgAddEvent(window, 'sgpbDidClose', function(e) {
		var args = e.detail;
		var popupId = parseInt(args.popupId);

		var iframeTag = jQuery('.sgpb-popup-builder-content-'+popupId+' .sgpb-iframe-'+popupId);
		if (iframeTag.length) {
			iframeTag.each(function() {
				jQuery(this).addClass('sgpb-iframe-spiner');
			});
		}
	});
};


SGPBPopupPro.prototype.init = function()
{
	this.eventListener();
};

jQuery(document).ready(function() {
	var obj = new SGPBPopupPro();
	obj.init();
});
